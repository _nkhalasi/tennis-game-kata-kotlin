package tech.njk.tennisgame

sealed class Player
object PlayerOne: Player()
object PlayerTwo: Player()

sealed class Point
object Love: Point()
object Fifteen: Point()
object Thirty: Point()

sealed class Score
data class Points(
    val playerOnePoint: Point,
    val playerTwoPoint: Point
): Score()
data class Forty(
    val player: Player,
    val otherPlayerPoint: Point
): Score()
object Deuce: Score()
data class Advantage(val player: Player): Score()
data class Game(val player: Player): Score()