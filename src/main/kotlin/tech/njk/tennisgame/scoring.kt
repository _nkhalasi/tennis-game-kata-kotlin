package tech.njk.tennisgame

import arrow.core.None
import arrow.core.Option
import arrow.core.Some
import arrow.core.some

typealias ComputeScore = (Score, Player) -> Score

private fun Point.increment(): Option<Point> =
  when (this) {
    Love -> Fifteen.some()
    Fifteen -> Thirty.some()
    Thirty -> None
  }

private fun scoreWhenDeuce(pointWinner: Player): Score = Advantage(pointWinner)

private fun scoreWhenAdvantage(advantagePlayer: Player, pointWinner: Player): Score =
  if (advantagePlayer == pointWinner) Game(pointWinner) else Deuce

private fun scoreWhenForty(currentScore: Forty, pointWinner: Player): Score =
  if (currentScore.player == pointWinner)
    Game(pointWinner)
  else {
    when (val nextPoint = currentScore.otherPlayerPoint.increment()) {
      None -> Deuce
      is Some -> Forty(currentScore.player, nextPoint.t)
    }
  }

private fun Points.pointsFor(player: Player): Point =
  when(player) {
    PlayerOne -> this.playerOnePoint
    PlayerTwo -> this.playerTwoPoint
  }

private fun Points.pointsTo(player: Player, point: Point): Points =
  when (player) {
    PlayerOne -> Points(point, this.playerTwoPoint)
    PlayerTwo -> Points(this.playerOnePoint, point)
  }

private fun Points.otherPlayer(player: Player): Player =
  when (player) {
    PlayerOne -> PlayerTwo
    PlayerTwo -> PlayerOne
  }

private fun scoreWhenPoints(currentScore: Points, pointWinner: Player): Score {
  return when (val newPoint = currentScore.pointsFor(pointWinner).increment()) {
    None -> Forty(pointWinner, currentScore.pointsFor(currentScore.otherPlayer(pointWinner)))
    is Some -> currentScore.pointsTo(pointWinner, newPoint.t)
  }
}

private fun scoreWhenGame(pointWinner: Player): Score = Game(pointWinner)

fun computeScore(currentScore: Score, pointWinner: Player): Score =
  when (currentScore) {
    is Advantage -> scoreWhenAdvantage(currentScore.player, pointWinner)
    is Deuce -> scoreWhenDeuce(pointWinner)
    is Forty -> scoreWhenForty(currentScore, pointWinner)
    is Points -> scoreWhenPoints(currentScore, pointWinner)
    is Game -> scoreWhenGame(pointWinner)
  }
