package tech.njk.tennisgame

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class ScoringTests {
  @Test
  fun deuceToAdvantage() {
    assertEquals(computeScore(Deuce, PlayerOne), Advantage(PlayerOne))
    assertEquals(computeScore(Deuce, PlayerTwo), Advantage(PlayerTwo))
  }

  @Test
  fun advantageToDeuce() {
    assertEquals(computeScore(Advantage(PlayerOne), PlayerTwo), Deuce)
    assertEquals(computeScore(Advantage(PlayerTwo), PlayerOne), Deuce)
  }

  @Test
  fun advantageToGame() {
    assertEquals(computeScore(Advantage(PlayerOne), PlayerOne), Game(PlayerOne))
    assertEquals(computeScore(Advantage(PlayerTwo), PlayerTwo), Game(PlayerTwo))
  }

  @Test
  fun scoresWhenForty() {
    assertEquals(computeScore(Forty(PlayerOne, Love), PlayerOne), Game(PlayerOne))
    assertEquals(computeScore(Forty(PlayerOne, Fifteen), PlayerOne), Game(PlayerOne))
    assertEquals(computeScore(Forty(PlayerOne, Thirty), PlayerOne), Game(PlayerOne))
    assertEquals(computeScore(Forty(PlayerOne, Love), PlayerTwo), Forty(PlayerOne, Fifteen))
    assertEquals(computeScore(Forty(PlayerOne, Fifteen), PlayerTwo), Forty(PlayerOne, Thirty))
    assertEquals(computeScore(Forty(PlayerOne, Thirty), PlayerTwo), Deuce)
  }

  @Test
  fun incrementScores() {
    assertEquals(computeScore(Points(Love, Love), PlayerOne), Points(Fifteen, Love))
    assertEquals(computeScore(Points(Thirty, Love), PlayerTwo), Points(Thirty, Fifteen))
    assertEquals(computeScore(Points(Thirty, Fifteen), PlayerOne), Forty(PlayerOne, Fifteen))
  }
}