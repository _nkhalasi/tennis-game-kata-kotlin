package tech.njk.tennisgame

import arrow.syntax.function.andThen
import arrow.syntax.function.pipe

val newGame = Points(Love, Love)

//fun getName() = "Naresh"
//fun print_(msg: String) = println(msg)

fun main() {
//  val getAndPrint = ::getName andThen ::print_
//  getAndPrint()
//
//  getName().pipe(::print_)

  val s = newGame
    .pipe{ computeScore(it, PlayerOne) }
    .pipe { computeScore(it, PlayerOne) }
    .pipe { computeScore(it, PlayerTwo) }
    .pipe { computeScore(it, PlayerTwo) }
    .pipe { computeScore(it, PlayerTwo) }
  println(s)
  val s1 = s.pipe { computeScore(it, PlayerOne) }
  val s2 = s1.pipe { computeScore(it, PlayerTwo) }
  println(s1)
  println(s2)
  val s3 = s2.pipe { computeScore(it, PlayerTwo) }
  println(s3)
  val s4 = s3.pipe { computeScore(it, PlayerTwo) }
  println(s4)

  println("--**--".repeat(15))
  val wins = listOf(PlayerOne, PlayerOne, PlayerTwo, PlayerTwo, PlayerTwo, PlayerOne, PlayerTwo, PlayerOne, PlayerTwo)
  val s5 = wins.fold(newGame, ::computeScore)
  println(s5)
}
