package tech.njk.tennisgame

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class PointsTest {
    @Test
    fun bothPlayersAtZeroPoints() {
        val loveAll = Points(Love, Love)
        assertEquals(loveAll.playerOnePoint, loveAll.playerTwoPoint)
    }

    @Test
    fun zeroFifteenPoints() {
        val combination1 = Points(Love, Fifteen)
        val combination2 = Points(Fifteen, Love)
        assertEquals(combination1.playerOnePoint, combination2.playerTwoPoint)
        assertEquals(combination1.playerTwoPoint, combination2.playerOnePoint)
    }
}
