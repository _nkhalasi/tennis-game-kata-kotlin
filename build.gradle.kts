import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
  kotlin("jvm") version "1.3.50"
  kotlin("kapt") version "1.3.50"
}

group = "tech.njk"
version = "1.0-SNAPSHOT"

val arrowVersion = "0.9.0"

repositories {
  jcenter()
  mavenCentral()
  mavenLocal()
}

dependencies {
  runtime(kotlin("reflect"))
  implementation(kotlin("stdlib"))

  compile("io.arrow-kt", "arrow-core-data", arrowVersion)
  compile("io.arrow-kt", "arrow-core-extensions", arrowVersion)
  compile("io.arrow-kt", "arrow-syntax", arrowVersion)
  compile("io.arrow-kt", "arrow-typeclasses", arrowVersion)
  compile("io.arrow-kt", "arrow-extras-data", arrowVersion)
  compile("io.arrow-kt", "arrow-extras-extensions", arrowVersion)
  kapt("io.arrow-kt", "arrow-meta", arrowVersion)

  testCompile("org.junit.jupiter", "junit-jupiter-engine", "5.5.2")
  testRuntime("org.junit.jupiter", "junit-jupiter-engine", "5.5.2")
}

tasks.test {
  useJUnitPlatform()
  testLogging {
    events("passed", "skipped", "failed")
  }
}
tasks.withType<KotlinCompile> {
  kotlinOptions.jvmTarget = "1.8"
}